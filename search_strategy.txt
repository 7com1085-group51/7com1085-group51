Research Question:

What are the privacy issues and security challenges that the internet of cloud is facing today and how can the internet of cloud be protected from these security threats?


Search Strings:

- "internet of cloud" AND security AND (vulnerabilities OR threat OR "data control" OR "cloud service") > 131 matches

-  security AND Internet of cloud technology AND privacy AND (threats OR vulnerabilities)  > 172 matches

- "internet of cloud" AND ("data security" OR "data protection" OR "data control") > 37 matches

- "internet of cloud" AND cloud security AND (vulnerabilities OR threat OR "data control" OR "cloud service") > 42 matches

- "internet of cloud" AND (security OR vulnerabilities OR threat OR "data control" OR "cloud service") > 234 matches

- "internet of cloud" AND security AND (vulnerabilities OR threat OR "data control" OR "data protection OR "cloud service" OR "cloud technology") > 52 matches

- countermeasures AND ("internet of cloud" OR "cloud security") > 43 matches



Dismissed Search Strings:

- internet of cloud AND Privacy AND (Expliotable OR Vulnerability OR Threat) AND (contermeasures OR mitigation) > 15 matches

- (exploit OR threat) AND "privacy issues" AND ("Internet of cloud" OR "internet of things") > 275 matches

- (exploit OR threat) AND privacy AND ("Internet of cloud" OR "internet of things") > (811 matches)

- (exploit OR threat OR "privacy issues") AND "Internet of cloud" > 62 matches

- "internet of cloud security" AND ("data control" OR "data protection" OR "cloud service" OR "cloud technology" OR vulnerabilities OR threat) > 1 match

- "Emerging security" AND privacy challenges AND internet of cloud technologies AND (threats OR vulnerabilities) > 1 match

("Emerging security" OR "privacy challenges" OR "cloud security" OR threats OR vulnerabilities) > (46,228 matches)

"Emerging security issues" OR privacy challenges AND "internet of cloud" AND (threats OR vulnerabilities > (15 matches)

"internet of cloud" AND ("security issues" OR "privacy challenges") AND (threat OR vulnerability) > 7 Matches

"internet of cloud" AND ("security issues" OR "privacy challenges" OR threat OR vulnerability) > 51 matches



Inclusion Criteria:

- Books, Papers and Journals that matches the context of the research question under consideration.

- Any study that identifies the current privacy issues and security challenges faced by the internet of cloud technology.

- Studies that analyzes the potential threats to the security of internet of cloud.  

- Studies that suggest practical solutions and countermeasures to attacks against the internet of cloud 

- Studies that suggest models and frameworks on how the internet of cloud can be protected from security threats.



Exclusion criteria:

- Books, Papers and Journals that does not match the context of the research question under consideration

- Studies that does not explain the emerging privacy and security challenges
 
- Studies that does not entail practical implementation and procedures to secure internet of cloud. 

Sources/search engines:
- IEEE Xplore (https://ieeexplore.ieee.org/)
- Google Scholar (https://scholar.google.com/)
- semantic scholar (https://www.semanticscholar.org/)
- ACM Digital Library (https://dl.acm.org/)
- Scopus (https://www.scopus.com/)
